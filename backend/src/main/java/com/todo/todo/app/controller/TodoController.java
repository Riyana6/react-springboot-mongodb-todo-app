package com.todo.todo.app.controller;

import com.todo.todo.app.service.TodoService;
import com.todo.todo.app.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
public class TodoController {

    @Autowired
    TodoService todoService;

    @PostMapping("/")
    public Todo save(@RequestBody Todo todo) {
        return todoService.save(todo);
    }

    @GetMapping("/")
    public List<Todo> getAll() {
        return todoService.getAll();
    }

    @GetMapping("/{id}")
    public Todo getById(@PathVariable(value= "id") String id) {
        return todoService.findById(id);
    }

    @PutMapping("/")
    public Todo update(@RequestBody Todo todo) {
        return todoService.update(todo);
    }

    @DeleteMapping("/{id}")
    public Todo delete(@PathVariable(value= "id") String id) {
        return todoService.delete(id);
    }
}
