import axios from 'axios';

const TODOLIST_API_BASE_URL = 'http://localhost:8080/api/auth/';

class TodoListService{

    getTodoList(){
        return axios.get(TODOLIST_API_BASE_URL);
    }
    deleteTodo(id){
        return axios.delete(TODOLIST_API_BASE_URL+id);
    }
    addTodo({todoText, todoStatus, dueDate, userName}){
        return axios.post(TODOLIST_API_BASE_URL,{todoText,todoStatus,dueDate,userName});
    }
    updateTodo(id){
        return axios.get(TODOLIST_API_BASE_URL+id);
    }
    editTodo({id,todoText, todoStatus, dueDate, userName}){
        return axios.put(TODOLIST_API_BASE_URL,{id,todoText, todoStatus, dueDate,userName});
    }
}

export default new TodoListService();