package com.todo.todo.app.repository;

import com.todo.todo.app.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TodoRepository {
    @Autowired
    MongoTemplate mongoTemplate;

    public Todo save(Todo todo) {
        return mongoTemplate.save(todo);
    }

    public List<Todo> find() {
        return mongoTemplate.findAll(Todo.class);
    }

    public Todo delete(String id) {
        Query query= new Query(Criteria.where("_id").is(id));
        return mongoTemplate.findAndRemove(query, Todo.class);
    }

    public Todo findById(String id) {
        Query query= new Query(Criteria.where("_id").is(id));
        return mongoTemplate.findOne(query,Todo.class);
    }
}
