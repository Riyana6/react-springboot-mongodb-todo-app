package com.todo.todo.app.service;

import com.todo.todo.app.model.Todo;
import com.todo.todo.app.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TodoService {

    @Autowired
    TodoRepository todoRepository;

    public Todo save(Todo todo) {
        return todoRepository.save(todo);
    }

    public List<Todo> getAll() {
        return todoRepository.find();
    }

    public Todo update(Todo todo) {
        return todoRepository.save(todo);
    }

    public Todo delete(String id) {
        return todoRepository.delete(id);
    }

    public Todo findById(String id) {
        return todoRepository.findById(id);
    }
}
